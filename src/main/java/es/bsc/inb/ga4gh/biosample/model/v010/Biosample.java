/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.biosample.model.v010;

import javax.json.JsonValue;

/**
 * @author Dmitry Repchevsky
 */

public class Biosample {
    
    private String biosampleId;
    private String individualId;
    private String description;
    private Age individualAgeAtCollection;
    private SampleOrigin sampleOrigin;
    private String obtentionProcedure;
    private CancerFeatures cancerFeatures;
    private JsonValue info;
    
    public String getBiosampleId() {
        return biosampleId;
    }

    public void setBiosampleId(final String biosampleId) {
        this.biosampleId = biosampleId;
    }
    
    public String getIndividualId() {
        return individualId;
    }

    public void setIndividualId(final String individualId) {
        this.individualId = individualId;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
    
    public Age getIndividualAgeAtCollection() {
        return individualAgeAtCollection;
    }

    public void setIndividualAgeAtCollection(final Age individualAgeAtCollection) {
        this.individualAgeAtCollection = individualAgeAtCollection;
    }

    public SampleOrigin getSampleOrigin() {
        return sampleOrigin;
    }

    public void setSampleOrigin(final SampleOrigin sampleOrigin) {
        this.sampleOrigin = sampleOrigin;
    }
    
    public String getObtentionProcedure() {
        return obtentionProcedure;
    }

    public void setObtentionProcedure(final String obtentionProcedure) {
        this.obtentionProcedure = obtentionProcedure;
    }
    
    public CancerFeatures getCancerFeatures() {
        return cancerFeatures;
    }

    public void setCancerFeatures(final CancerFeatures cancerFeatures) {
        this.cancerFeatures = cancerFeatures;
    }
    
    public JsonValue getInfo() {
        return info;
    }

    public void setInfo(final JsonValue info) {
        this.info = info;
    }
}
